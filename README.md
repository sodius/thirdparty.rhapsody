# Prepare and create a new release of Rhapsody API

* Install the new Rhapsody kit, for example **Rhapsody 9.0.2**, only the application for **64 bits** architecture.

* Clone the Git project in your work

* Select the **master** branch and pull the latest commit.

* Open a **File Explorer** into folder `C:\Program Files\IBM\Rhapsody\9.0.2\Share\JavaAPI`

* Copy the whole content of `C:\Program Files\IBM\Rhapsody\9.0.2\Share\JavaAPI` including **WIN32** folder and also its content into folder:  

`.\thirdparty.rhapsody\src\rhapsody\lib` (Git project cloned on local machine)

* Open this file and copy text of the whole version with its build number:  

`C:\Program Files\IBM\Rhapsody\9.0.2\BuildNo.txt`

* Edit the launch configuration **UpdateProjectTPRHP**, edit parameter `newVersion` in **Main** tab and past the text containing the whole version, for example `9.0.2.R00_202211152158`

* Launch the configuration **UpdateProjectTPRHP** and check that several `pom.xml`, `category.xml, the `UpdateProjectTPRHP.launch` and one `manifest.mf` are updated.
  
* Commit on the **master** branch all changes including `.dll` files (a total of 11 files).

* Launch a **Teamcity** build on this **master** branch.

* Launch a TC **Deploy** on master branch and check that a new release is created **9.0.2.R00_202211152158**.

